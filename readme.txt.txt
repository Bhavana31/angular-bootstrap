You need to set up your development environment before starting with Angular.
------------------------------------------------------------------------------------
Open the integrated terminal in the view and run the following command : 

npm install -g @angular/cli -> It installs the Angular CLI globally.
The Angular CLI installs the necessary npm packages, creates the project files, and populates the project with a simple default app. This can take some time.

npm install bootstrap -> It installs Bootstrap with necessary packages.

npm install chart.js -> It install the necessary packages to view the charts.

-------------------------------------------------------------------------------------

Go to the project directory where the tenderproject is saved and launch the server:
file -> open -> tenderproject

Open the integrated terminal:
cd <folder-name> -> location where tenderproject is saved. 

ng serve --open
Using the --open option will automatically open your browser on http://localhost:4200/.

--------------------------------------------------------------------------------------
For DB connectivity :
Install xampp -> store the php files from DB connectivity folder to htdocs
go to localhost/phpmyadmin/ -> and export the tenderdata.sql file to import all necessary data.

----------------------------------------------------------------------------------------








