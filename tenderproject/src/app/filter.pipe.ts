import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(arrdata:any[],searchText:string) :any{
    if (!arrdata || !searchText) {
    return arrdata;
    }
    return arrdata.filter(arrdata => 
      arrdata.location.toLowerCase().indexOf(searchText) > -1
  );

}
}

