import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ChartService } from '../chart.service';
import { HttpClient, HttpErrorResponse } from '../../../node_modules/@angular/common/http';


@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {

  chart2=[];

  constructor(private _chart: ChartService,private http :HttpClient) { }

  ngOnInit() {
    (err : HttpErrorResponse )=>{
      console.log(err.message);
    }

    this._chart.dailyForecast()
    .subscribe(res => {
      const earnings_per_share = res['list'].map(res => res.main.earnings_per_share);
      const EPS_forecast = res['list'].map(res => res.main.EPS_forecast);
      const alldates = res['list'].map(res => res.dt);

      const weatherDates = [];
      alldates.forEach((res) => {
      const jsdate = new Date(res * 1000);
      weatherDates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }));
    });


this.chart2 = new Chart('canvas1', {
  type: 'bar',
  data: {
    labels: weatherDates,
    datasets: [
      {
        label: 'Earnings per Share',
        data: earnings_per_share,
        backgroundColor : '#ec5525',
        borderColor: '#ec5525',
        fill: false
      },
      {
        label: 'EPS Forecast',
        data: EPS_forecast,
        backgroundColor : '#34434e',
        borderColor: '#34434e',
        fill: false
      },
    ]
  },
  options: {
    legend: {
      display: true,
      fullWidth: true
    },
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Dates',
          fontStyle: 'bold',
          fontColor: 'red'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Earnings per Share & EPS Forecast',
          fontStyle: 'bold',
          fontColor: 'red'
        }
      }],
    }
  }
});
});




  }

}
