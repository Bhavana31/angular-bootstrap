import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '../../../node_modules/@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  arrdata: string[];
  arrdata2:string[];
  arrdata3:string[];
  arrdata4:string[];
  isActive=false;
  strdata: string[];
  strdata2:string[];
  strdata3:string[];
  strdata4:string[];
  public url=("http://localhost/angular/inbox.php");
  public url2=("http://localhost/angular/starred.php");
  public url3=("http://localhost/angular/important.php");
  public url4=("http://localhost/angular/trash.php");


  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get(this.url).subscribe(data=>{
      this.arrdata = data as string[];
    },
  (err : HttpErrorResponse)=>{
    console.log(err.message);
  })
  this.http.get('http://localhost/angular/inbox.php').subscribe(data=>{
    this.strdata = data as string[];
  });

  this.http.get(this.url2).subscribe(data2=>{
    this.arrdata2 = data2 as string[];
  },
  (err : HttpErrorResponse)=>{
    console.log(err.message);
  })
  this.http.get('http://localhost/angular/starred.php').subscribe(data2=>{
    this.strdata2 = data2 as string[];
  });

  
  this.http.get(this.url3).subscribe(data3=>{
    this.arrdata3 = data3 as string[];
  },
  (err : HttpErrorResponse)=>{
    console.log(err.message);
  })
  this.http.get('http://localhost/angular/important.php').subscribe(data3=>{
    this.strdata3 = data3 as string[];
  });


  this.http.get(this.url4).subscribe(data4=>{
    this.arrdata4 = data4 as string[];
  },
  (err : HttpErrorResponse)=>{
    console.log(err.message);
  })
  this.http.get('http://localhost/angular/trash.php').subscribe(data4=>{
    this.strdata4 = data4 as string[];
  });



}
}