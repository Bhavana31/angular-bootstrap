import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '../../../node_modules/@angular/common/http';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  
 // searchText:string[];
  arrdata: string[];
  isActive=false;
  public url=("http://localhost/angular/inbox.php");

  constructor(private _http: HttpClient) { }


  ngOnInit() {
    
    this._http.get(this.url).subscribe(data=>{
      this.arrdata = data as string[];
    },
  (err : HttpErrorResponse)=>{
    console.log(err.message);
  });

  }

}
