-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2018 at 05:31 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `angular`
--

-- --------------------------------------------------------

--
-- Table structure for table `tenderdata`
--

CREATE TABLE `tenderdata` (
  `id` int(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `location` varchar(255) NOT NULL,
  `deadline` date NOT NULL,
  `important` int(2) NOT NULL,
  `starred` int(2) NOT NULL,
  `trash` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenderdata`
--

INSERT INTO `tenderdata` (`id`, `title`, `summary`, `date`, `location`, `deadline`, `important`, `starred`, `trash`) VALUES
(1, 'Water Software', 'Providing 3rd stage water supply scheme with Cauvery river as source. ', '2018-09-11', 'maharashtra', '2018-09-15', 1, 0, 0),
(2, 'Supply of system coal', 'Supply, Installation and Commissioning of 04 Nos of 10-12 CuM Diesel Hydraulic Backhoe Shovel, 01 No of 10-12 CuM Diesel Hydraulic Face Shove', '2018-10-23', 'gujrat', '2018-10-30', 0, 1, 0),
(3, 'Fabrication & shifting of miscellaneous items as per the attached specification. ', 'Silk', '2018-06-11', 'punjab', '2018-06-20', 0, 0, 1),
(4, 'Charter Hire', 'Procurement of charter hire', '2017-02-20', 'mumbai', '2018-08-30', 1, 1, 0),
(5, 'Sales', 'Procurement of sales', '2015-11-12', 'Haryana', '2018-04-15', 0, 0, 1),
(6, 'Medicine', 'Procurement of medicine ', '2017-03-16', 'mumbai', '2018-09-30', 1, 0, 1),
(7, 'Charter hire', 'Procurement of charter hire', '2017-12-05', 'maharashtra', '2018-10-19', 1, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tenderdata`
--
ALTER TABLE `tenderdata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tenderdata`
--
ALTER TABLE `tenderdata`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
